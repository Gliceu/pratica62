
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {

    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        JogadorComparator jc = new JogadorComparator();
        time1.addJogador("Goleiro", new Jogador(1, "Wilson"));
        time1.addJogador("Atacante", new Jogador(10, "Kleber"));
        time1.addJogador("Meia   ", new Jogador(8, "Ruan"));
        time1.addJogador("LarEsq  ", new Jogador(2, "Carlos"));
        time1.addJogador("LatDir  ", new Jogador(6, "Junior"));
        time1.addJogador("Volante  ", new Jogador(5, "Lucas"));
        List<Jogador> jog;
        jog = time1.ordena(jc);
        System.out.println(jog);
        Collections.binarySearch(jog, jog.get(0));

    }
}
