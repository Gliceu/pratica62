package utfpr.ct.dainf.if62c.pratica;

public class Jogador implements Comparable<Jogador> {

    int numero;
    String nome;

    public Jogador() {
    }

    public Jogador(int numero, String nome) {
        this.numero = numero;
        this.nome = nome;
    }

    public int compareTo(Jogador jog) {
        return this.nome.compareTo(jog.nome);
    }

    @Override
    public String toString() {
        return String.format("%4s-%s", numero, nome);
    }
}
