package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

public class JogadorComparator implements Comparator<Jogador> {

    public boolean porNumero = true;
    public boolean ascNum = true;
    public boolean ascNome = false;

    @Override
    public int compare(Jogador j1, Jogador j2) {
        if (porNumero) {
            if (ascNum) {
                if (j1.numero > j2.numero) {
                    return 1;
                }
                if (j1.numero < j2.numero) {
                    return -1;
                }
            } else {
                if (j1.numero < j2.numero) {
                    return 1;
                }
                if (j1.numero > j2.numero) {
                    return -1;
                }
            }
        }
        if (ascNome) {
            System.out.println("nada impementado");
        }
        return 0;
    }
}
