package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Time {

    private String nome;
    private int numero;

    public Time() {
    }

    public Time(Jogador jogador) {
    }

    public Time(int numero, String nome) {
        this.nome = nome;
        this.numero = numero;

    }
    public HashMap<String, Jogador> jogadores = new HashMap<>();

    public HashMap<String, Jogador> getJogadores() {

        return jogadores;
    }

    public void addJogador(String o, Jogador jogador) {

        jogadores.put(o, jogador);

    }

    public List ordena(JogadorComparator jogado) {
        List<Jogador> jog = new ArrayList<>(getJogadores().values());
        Collections.sort(jog);
      //  System.out.println("*" + jog);
        Collections.sort(jog, new JogadorComparator());
      //  System.out.println("**" + jog);

        return jog;
    }

}
